export class Money implements Expression {
  public amount: number;
  public _currency: string | any;

  constructor(amount: number, currency: string) {
    this.amount = amount;
    this._currency = currency;
  }

  static dollar(amount: number) {
    return new Dollar(amount, 'USD');
  }

  static franc(amount: number) {
    return new Franc(amount, 'CHF');
  }

  toString(amount: number): string {
    return amount + ' ' + this._currency;
  }

  currency(): Money {
    return this._currency;
  }

  public equals(money: Money): boolean {
    return this.amount === money.amount && this._currency === money.currency();
  }

  public plus(addend: Expression): Expression {
    return new Sum(this, addend);
  }

  times(multiplier: number): Expression {
    return new Money(this.amount * multiplier, this._currency);
  }

  public reduce(bank: Bank, to: string): Money {
    const rate = bank.rate(this._currency, to)
    return new Money(this.amount / rate, to)
  }

}

type Expression = {
  reduce(bank: Bank, to: string): Money
  plus(addend: Expression): Expression;
  times(multiplier: number): Expression;
};

export class Bank {
  private _rate: number

  constructor() {
    this._rate = 0
  }

  reduced(source: Expression, to: string): Money {
    if (source instanceof Money) {
      return source.reduce(this, to);
    }
    return source.reduce(this, to)
  }

  rate(from: string, to: string) {
    if (from === to) return 1
    return this._rate
  }


  addRate(from : string, to: string, rate: number): void {
    this.rate(from, to)
    this._rate = rate
  }
}

export class Sum {
  public augend: Expression;
  public addend: Expression;

  constructor(augend: Expression, addend: Expression) {
    this.augend = augend;
    this.addend = addend;
  }

  reduce(bank: Bank, to: string): Money {


    const amount = this.augend.reduce(bank, to).amount + this.addend.reduce(bank, to).amount;
    return new Money(amount, to);
  }

  public plus(addend: Expression): Expression {
    return new Sum(this, addend);
  }

  times(multiplier: number): Expression {
    return new Sum(this.augend.times(multiplier),this.addend.times(multiplier));
  }
}

export class Dollar extends Money {
  constructor(amount: number, currency: string) {
    super(amount, currency);

    return new Money(amount, 'USD');
  }
}

export class Franc extends Money {
  constructor(amount: number, currency: string) {
    super(amount, currency);
    return new Money(amount, 'CHF');
  }
}
