import { Bank, Dollar, Franc, Money, Sum } from './calc-number';

describe('Dolar', () => {
  it('Se é posivel multiplicar o dolar', () => {
    expect(new Money(10, 'USD')).toEqual(new Dollar(10, 'USD'));
  });

  it('se o dollar é igual a ele mesmo', () => {
    const checkDollar = new Money(5, 'USD').equals(new Dollar(5, 'USD'));
    const checkFranc = new Money(5, 'CHF').equals(new Franc(5, 'CHF'));

    expect(checkDollar).toBeTruthy();
    expect(checkFranc).toBeTruthy();
  });

  it('se a moeda não é igual a ele mesmo', () => {
    const checkDolar = new Money(5, 'USD').equals(new Dollar(6, 'USD'));
    const checkFranc = new Money(5, 'CHF').equals(new Franc(6, 'CHF'));

    expect(checkDolar).toBeFalsy();
    expect(checkFranc).toBeFalsy();
  });

  it('testSimpleAddition', () => {
    const sum = new Sum(Money.dollar(3), Money.dollar(4));

    const bank = new Bank();
    const result = bank.reduced(sum, 'USD');
    expect(Money.dollar(7)).toEqual(result);
  });

  it('testReduceMoney', () => {
    const sum = new Sum(Money.dollar(3), Money.dollar(4));

    const bank = new Bank();
    const result = bank.reduced(Money.dollar(1), 'USD');
    expect(Money.dollar(1)).toEqual(result);
  });

  it('testReduceMoneyDifferentCurrency()', () => {
    const bank = new Bank();
    bank.addRate('CHF' , "USD", 2);
    const result = bank.reduced(Money.franc(2), "USD")
    expect(Money.dollar(1)).toEqual(result);
  });

  it('testIdentityRate()', () => {
    expect(1).toEqual(new Bank().rate("USD", "USD"))
  })

  it('testSumPlusMoney()', () => {
    const fiveBucks= Money.dollar(5);
    const tenFrancs= Money.franc(10);
    const bank= new Bank();

    bank.addRate("CHF", "USD", 2);
    const sum = new Sum(fiveBucks, tenFrancs).times(2);
    const result= bank.reduced(sum, "USD");
    expect(Money.dollar(20)).toEqual(result);
  })
});